import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App";

test("performs a movie search with routing", async () => {
  render(<App />);

  const searchInput = screen.getByPlaceholderText("Search for movies...");
  const searchButton = screen.getByTestId("search-button");

  global.fetch = jest.fn().mockResolvedValue({
    json: () => Promise.resolve({ results: [{ title: "The Matrix" }] }),
  });

  act(() => {
    userEvent.type(searchInput, "The Matrix");
  });

  userEvent.click(searchButton);

  await waitFor(() => {
    const movieTitle = screen.getByText("The Matrix");
    expect(movieTitle).toBeInTheDocument();
  });

  (global.fetch as jest.Mock).mockRestore();
});
