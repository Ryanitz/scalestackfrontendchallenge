import { fireEvent, render, screen } from "@testing-library/react";
import SearchBar from "./SearchBar";

const mockProps = {
  onSearch: jest.fn(),
  isLoading: false,
  initialSearchQuery: "Mocked Query",
};

test("renders search input and button", () => {
  render(<SearchBar {...mockProps} />);
  const inputElement = screen.getByPlaceholderText("Search for movies...");
  const buttonElement = screen.getByText("Search");
  expect(inputElement).toBeInTheDocument();
  expect(buttonElement).toBeInTheDocument();
});

test("triggers onChange event for input", () => {
  render(<SearchBar {...mockProps} />);
  const inputElement = screen.getByPlaceholderText(
    "Search for movies..."
  ) as HTMLInputElement;

  fireEvent.change(inputElement, { target: { value: "New Value" } });

  expect(inputElement.value).toBe("New Value");
});

test("calls handleSearch() when searchQuery is cleared", () => {
  const mockOnSearch = jest.fn();
  render(
    <SearchBar
      onSearch={mockOnSearch}
      isLoading={false}
      initialSearchQuery="Mocked Query"
    />
  );
  const inputElement = screen.getByPlaceholderText("Search for movies...");

  fireEvent.change(inputElement, { target: { value: "" } });

  expect(mockOnSearch).toHaveBeenCalledWith("");
});

test('calls handleSearch() when "Enter" key is pressed', () => {
  const mockOnSearch = jest.fn();
  render(
    <SearchBar
      onSearch={mockOnSearch}
      isLoading={false}
      initialSearchQuery="Mocked Query"
    />
  );
  const inputElement = screen.getByPlaceholderText("Search for movies...");

  fireEvent.change(inputElement, { target: { value: "New Value" } });

  fireEvent.keyPress(inputElement, {
    key: "Enter",
    code: "Enter",
    charCode: 13,
  });

  expect(mockOnSearch).toHaveBeenCalledWith("New Value");
});

test("renders SearchBar with default props", () => {
  const { getByPlaceholderText } = render(<SearchBar />);
  const inputElement = getByPlaceholderText("Search for movies...");

  expect(inputElement).toBeInTheDocument();
});
