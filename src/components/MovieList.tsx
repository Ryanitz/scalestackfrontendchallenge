import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import "tailwindcss/tailwind.css";
import { Movie } from "../types";

interface MovieListProps {
  movies: Movie[];
}

const MovieList: React.FC<MovieListProps> = ({ movies }) => {
  useEffect(() => {
    console.log(movies);
  }, [movies]);

  return (
    <div className="flex justify-center my-8">
      <div className="w-full lg:w-2/3 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4">
        {movies.map((movie, index) => (
          <Link
            key={movie.id}
            to={`/movie/${movie.id}`}
            className={`group bg-white ${
              index === 0 ? "col-span-2 row-span-2" : "col-span-1 row-span-1"
            } relative`}
          >
            <img
              src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`}
              alt={`Poster for ${movie.title}`}
              className="w-full h-full border rounded shadow-md"
            />

            <div className="absolute bottom-0 left-0 right-0 bg-black text-white opacity-0 group-hover:opacity-75 p-2 transition-opacity duration-300">
              {movie.title}
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default MovieList;
