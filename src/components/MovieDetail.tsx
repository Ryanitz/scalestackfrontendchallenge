import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { Movie } from "../types";

const MovieDetail: React.FC = () => {
  const { movieId } = useParams<{ movieId: string }>();
  const [movie, setMovie] = useState<Movie | null>(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function fetchMovieDetails() {
      try {
        const response = await fetch(
          `https://api.themoviedb.org/3/movie/${movieId}?api_key=a24db5b0cd382b27ea2c9219066f9860`
        );
        const data = await response.json();
        setMovie(data);
        setIsLoading(false);
      } catch (error) {
        console.error("Error fetching movie details:", error);
        setIsLoading(false);
      }
    }

    fetchMovieDetails();
  }, [movieId]);

  if (isLoading) {
    return (
      <div className="flex items-center justify-center h-screen">
        <div className="animate-spin rounded-full h-32 w-32 border-t-4 border-blue-500"></div>
      </div>
    );
  }

  if (!movie) {
    return <div className="text-center">Movie not found</div>;
  }

  return (
    <div className="flex justify-center my-8">
      <div className="w-full lg:w-2/3 border bg-white p-4 rounded-lg shadow-md">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="flex flex-col">
            <Link
              to="/"
              className="w-fit mb-4 text-blue-500 border border-blue-500 rounded px-2 transition-all hover:bg-blue-500 hover:text-white"
            >
              Go Back
            </Link>
            <img
              src={`https://image.tmdb.org/t/p/w300/${movie.poster_path}`}
              alt={movie.title}
              className="rounded-lg shadow-md"
            />
          </div>
          <div>
            <h2 className="text-3xl mb-4 font-semibold text-blue-500">
              {movie.title}
            </h2>
            <p className="text-gray-700 text-lg">{movie.overview}</p>
            <div className="mt-4">
              <p className="text-blue-500 font-semibold">Release Date:</p>
              <p>{movie.release_date}</p>
              <p className="text-blue-500 font-semibold mt-2">Rating:</p>
              <p>{movie.vote_average}</p>
              <p className="text-blue-500 font-semibold mt-2">Genres:</p>
              <p>
                {movie.genres.map((genre) => (
                  <span
                    key={genre.id}
                    className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2"
                  >
                    {genre.name}
                  </span>
                ))}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieDetail;
