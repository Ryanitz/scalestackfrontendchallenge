import React from "react";
import { render, screen } from "@testing-library/react";
import MovieList from "./MovieList";
import { Movie } from "../types";
import { BrowserRouter } from "react-router-dom";

const mockMovies: Movie[] = [
  {
    id: 1,
    poster_path: "path1",
    title: "Movie 1",
    overview: "This is the overview for Movie 1",
    backdrop_path: "backdrop1",
    release_date: "2021-01-01",
    vote_average: 7.5,
    genres: [
      { id: 1, name: "Action" },
      { id: 2, name: "Adventure" },
    ],
  },
  {
    id: 2,
    poster_path: "path2",
    title: "Movie 2",
    overview: "This is the overview for Movie 2",
    backdrop_path: "backdrop2",
    release_date: "2021-02-01",
    vote_average: 8.0,
    genres: [
      { id: 3, name: "Drama" },
      { id: 4, name: "Romance" },
    ],
  },
];

test("renders movie posters", () => {
  render(
    <BrowserRouter>
      {" "}
      <MovieList movies={mockMovies} />
    </BrowserRouter>
  );

  mockMovies.forEach((movie) => {
    const altText = `Poster for ${movie.title}`;
    const posterElement = screen.getByAltText(altText);
    expect(posterElement).toBeInTheDocument();
  });
});
