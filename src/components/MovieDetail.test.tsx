import { render, screen } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import MovieDetail from "./MovieDetail";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({
    movieId: "123",
  }),
}));

test("renders movie details", async () => {
  render(
    <MemoryRouter initialEntries={["/movie/123"]}>
      <Routes>
        <Route path="/movie/:movieId" element={<MovieDetail />} />
      </Routes>
    </MemoryRouter>
  );

  const movieTitle = await screen.findByText(/The Lord of the Rings/i);

  expect(movieTitle).toBeInTheDocument();
});
