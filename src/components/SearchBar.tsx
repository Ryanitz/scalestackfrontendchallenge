import React, { useEffect, useState } from "react";

interface SearchBarProps {
  onSearch?: (query: string) => void;
  isLoading?: boolean;
  initialSearchQuery?: string;
}

const SearchBar: React.FC<SearchBarProps> = ({
  onSearch = () => {},
  isLoading = false,
  initialSearchQuery = "",
}) => {
  const [searchQuery, setSearchQuery] = useState(initialSearchQuery);
  const [prevSearchQuery, setPrevSearchQuery] = useState(initialSearchQuery);

  const handleSearch = () => {
    onSearch(searchQuery);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  useEffect(() => {
    if (searchQuery === "" && prevSearchQuery !== "") {
      handleSearch();
    }
    setPrevSearchQuery(searchQuery);
  }, [searchQuery, prevSearchQuery]);

  return (
    <div className="bg-white p-4 pb-8 rounded-md shadow-md my-2 flex">
      <input
        type="text"
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
        onKeyPress={handleKeyPress}
        placeholder="Search for movies..."
        className="w-full px-3 py-2 rounded-md border border-gray-300 focus:outline-none focus:ring focus:border-blue-300"
      />
      <button
        onClick={handleSearch}
        data-testid="search-button"
        className="ml-2 bg-blue-500 hover:bg-blue-600 text-white font-semibold px-4 py-2 rounded-md focus:outline-none focus:ring focus:ring-blue-300"
      >
        {isLoading ? (
          <div className="animate-spin rounded-full h-5 w-5 border-t-2 border-white"></div>
        ) : (
          "Search"
        )}
      </button>
    </div>
  );
};

export default SearchBar;
