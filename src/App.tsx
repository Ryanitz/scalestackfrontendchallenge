import React, { useEffect, useState } from "react";
import "./App.css";
import MovieList from "./components/MovieList";
import MovieDetail from "./components/MovieDetail";
import SearchBar from "./components/SearchBar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Movie } from "./types";

const App: React.FC = () => {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [query, setQuery] = useState("");
  const API_KEY = "a24db5b0cd382b27ea2c9219066f9860";

  const fetchMovies = async (query: string) => {
    setQuery(query);
    let url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${query}`;
    if (query === "")
      url = `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&sort_by=popularity.desc`;

    try {
      setIsLoading(true);
      const response = await fetch(url);
      const data = await response.json();
      setMovies(data.results);
      setIsLoading(false);
    } catch (error) {
      console.error("Error fetching movies:", error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchMovies("");
  }, []);

  return (
    <BrowserRouter>
      <div className="App">
        <h1 className="mt-6 text-xl font-bold">Movie Theater</h1>
        <Routes>
          <Route
            path="/"
            element={
              <SearchBar
                onSearch={fetchMovies}
                isLoading={isLoading}
                initialSearchQuery={query}
              />
            }
          />
          <Route path="/movie/:movieId" element={<MovieDetail />} />
        </Routes>

        <Routes>
          <Route
            path="/"
            element={
              <>
                <MovieList movies={movies} />
              </>
            }
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
};

export default App;
